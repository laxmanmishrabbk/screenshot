  
<?php


   function TakeScreenshot($element=null) {
        // Change the Path to your own settings
        $screenshot = $this->TempDirectoryPath . time() . ".png";

        // Change the driver instance
        $this->driver->takeScreenshot($screenshot);
        if(!file_exists($screenshot)) {
            throw new Exception('Could not save screenshot');
        }
        
        if( ! (bool) $element) {
            return $screenshot;
        }
        
        $element_screenshot = $this->TempDirectoryPath . time() . ".png"; // Change the path here as well
        
        $element_width = $element->getSize()->getWidth();
        $element_height = $element->getSize()->getHeight();
        
        $element_src_x = $element->getLocation()->getX();
        $element_src_y = $element->getLocation()->getY();
        
        // Create image instances
        $src = imagecreatefrompng($screenshot);
        $dest = imagecreatetruecolor($element_width, $element_height);

        // Copy
        imagecopy($dest, $src, 0, 0, $element_src_x, $element_src_y, $element_width, $element_height);
        
        imagepng($dest, $element_screenshot);
        
        // unlink($screenshot); // unlink function might be restricted in mac os x.
        
        if( ! file_exists($element_screenshot)) {
            throw new Exception('Could not save element screenshot');
        }
        
        return $element_screenshot;
    }
	
	
    $full_screenshot = TakeScreenshot();
    $screenshot_of_element = TakeScreenshot($this->driver->findElement(WebDriverBy::xpath("//img[@class='test']"));

	
	?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
	<div class="container">
		<div class="row m-3" >
			<div class="col-12" id="capture">
			<p>Drag on the larger image to select crop area.</p>
			<p>
				<img id="filePreview" src="myscreenshot.png">
			 </p>
			</div>
			<div class="col-12 mt-5">
			<form action="upload.php" enctype="multipart/form-data" method="post" onsubmit="return checkCoords();">
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
				<input name="upload" type="submit" value="Crop" />
			</form>
			</div>
		</div>
	
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-default.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script>
//set image coordinates
function updateCoords(im,obj){
    $('#x').val(obj.x1);
    $('#y').val(obj.y1);
    $('#w').val(obj.width);
    $('#h').val(obj.height);
}

//check coordinates
function checkCoords(){
    if(parseInt($('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
}

$(document).ready(function(){
    //prepare instant image preview
    var p = $("#filePreview");
    $("#fileInput").change(function(){
        //fadeOut or hide preview
        p.fadeOut();

        //prepare HTML5 FileReader
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("fileInput").files[0]);

        oFReader.onload = function (oFREvent) {
            p.attr('src', oFREvent.target.result).fadeIn();
        };
    });

    //implement imgAreaSelect plugin
    $('img#filePreview').imgAreaSelect({
        // set crop ratio (optional)
        //aspectRatio: '2:1',
        onSelectEnd: updateCoords
    });
});
</script> 
  <script>
  
  
  function capture(){
  html2canvas(document.querySelector("#capture")).then(canvas => {
    document.body.appendChild(canvas)
});
}
$("button").click(function(){
    $.ajax({url: "ajax.php", success: function(result){
       // $("#div1").html(result);
    }});
});
  </script>
  


   </body>
</html>